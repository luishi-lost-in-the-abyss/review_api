
const crypto = require("crypto"),
  algorithm = 'aes-256-gcm',
  password = '3zTvzr3p67VC61jmV54rIYu1545x4TlY',
  iv = '60iP0h6vJoEa';
const encrypt = text => {
  var cipher = crypto.createCipheriv(algorithm, password, iv);
  var encrypted = cipher.update(text, "utf8", "hex");
  encrypted += cipher.final("hex");
  var tag = cipher.getAuthTag();
  return encrypted;
};
const decrypt = encrypted => {
  var decipher = crypto.createDecipheriv(algorithm, password, iv);
  var dec = decipher.update(encrypted, "hex", "utf8");
  return dec;
};
module.exports = { encrypt, decrypt };