const express = require("express")
const cors = require("cors");

const {
  c_insertTransaction,
  c_selectAllTransactions
} = require("./controllers/transactions/index")

const {
  c_selectOneEmployee,
  c_importEmployees,
  c_employeeLogin,
  c_seedAdmin,
  c_changePassword
} = require("./controllers/employees/index")

const {
  c_selectAllQuestions
} = require("./controllers/questions/index")

const { verifyTokens } = require("./token/index")

const makeExpressCallback = require("./express-callback/index")


const app = express()

app.use(cors())

app.use(express.json())
app.use((_, res, next) => {
  res.set({ Tk: "!" })
  next();
})
app.put("/api/changepassword", verifyTokens, makeExpressCallback(c_changePassword))

app.put("/api/login", makeExpressCallback(c_employeeLogin))

app.post("/api/transactions/insert", makeExpressCallback(c_insertTransaction))
app.get("/api/transactions/select", verifyTokens, makeExpressCallback(c_selectAllTransactions))

app.post("/api/employees/select/:id", makeExpressCallback(c_selectOneEmployee))
app.post("/api/employees/import", makeExpressCallback(c_importEmployees))
app.post("/api/emplooyees/seed-admin", makeExpressCallback(c_seedAdmin))

app.get("/api/questions/select", makeExpressCallback(c_selectAllQuestions))


const PORT = process.env.PORT || 5000



app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}.`)
})



module.exports = app;
