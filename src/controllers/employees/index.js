

const selectOneEmployee = require("./employees-select-one")
const importEmployees = require("./employees-import")
const employeeLogin = require("./employees-login")
const seedAdmin = require("./employees-seed-admin")
const changePassword = require("./employees-change-password")


const importEmployeesFunction = require("../../import/import-employees")

const {
  uc_selectOneEmployee,
  uc_importEmployees,
  uc_employeeLogin,
  uc_seedAdmin,
  uc_changePassword
} = require("../../use-cases/employees/index");

const c_selectOneEmployee = selectOneEmployee({ uc_selectOneEmployee });
const c_importEmployees = importEmployees({uc_importEmployees, importEmployeesFunction})
const c_employeeLogin = employeeLogin({ uc_employeeLogin })
const c_seedAdmin = seedAdmin({ uc_seedAdmin })
const c_changePassword = changePassword({uc_changePassword})

const controller = Object.freeze({
  c_selectOneEmployee,
  c_importEmployees,
  c_employeeLogin,
  c_seedAdmin,
  c_changePassword
});


module.exports = controller;
module.exports = {
  c_selectOneEmployee,
  c_importEmployees,
  c_employeeLogin,
  c_seedAdmin,
  c_changePassword
};
