const selectAllQuestions = require("./questions-select-all")


const {
  uc_selectAllQuestions
} = require("../../use-cases/questions/index");

const c_selectAllQuestions = selectAllQuestions({ uc_selectAllQuestions });

const controller = Object.freeze({
    c_selectAllQuestions
});


module.exports = controller;
module.exports = {
    c_selectAllQuestions
};
