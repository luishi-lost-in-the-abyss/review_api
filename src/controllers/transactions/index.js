const insertTransaction = require("./transaction-insert")
const selectAllTransactions = require("./transaction-select-all")

const {
  uc_insertTransaction,
  uc_selectAllTransactions
} = require("../../use-cases/transactions/index");

const c_insertTransaction = insertTransaction({ uc_insertTransaction });
const c_selectAllTransactions = selectAllTransactions({uc_selectAllTransactions})

const controller = Object.freeze({
  c_insertTransaction,
  c_selectAllTransactions
});


module.exports = controller;
module.exports = {
  c_insertTransaction,
  c_selectAllTransactions
};
