const makeToken = ({ jwt }) => {
    return async function token(info) {
      // credentials of  the user
      const id = info.employeeid;
      const password = info.password;
      const user = {
        id,
        password
      };
      const token = await generateToken(jwt, user);
      return token;
    };
  };
  // generate token from user info then return
  const generateToken = async (jwt, user) => {
    try {
      const token = await jwt.sign(
        { user },
        'secret' /*, { expiresIn: "30s" }*/
      );
      return token;
    } catch (e) {
      console.log("Error in generating token: ", e);
    }
  };
  module.exports = makeToken;