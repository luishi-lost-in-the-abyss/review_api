const verifyToken = ({ jwt, accountsDb, encrypt }) => {
    return async function token(req, res, next) {
      const bearerHeader = req.headers["authorization"];
      if (typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
        const userData = await getUserDataThruToken(req.token, jwt);
        if (userData) {
          const queryToken = await accountsDb.selectCredentials({
            employeeid: userData.user.id,
            password: encrypt(userData.user.password)
          });
          // if token doesn't exist
          if (queryToken.rows.length === 0) {
            res.sendStatus(403);
            return;
          }
          const dbToken = queryToken.rows[0].token;
          if (dbToken === req.token) {
            jwt.verify(req.token, 'secret', (err, authData) => {
              if (err) {
                res.sendStatus(403);
              } else {
                next();
              }
            });
          } else {
            res.sendStatus(403);
          }
        } else {
          res.sendStatus(403);
        }
      } else {
        res.sendStatus(403);
      }
    };
  };
  const getUserDataThruToken = async (token, jwt) => {
    let authData = "";
    await jwt.verify(token, 'secret', (err, authorizedData) => {
      if (!err) {
        authData = authorizedData;
      }
    });
    return authData;
  };
  module.exports = verifyToken;