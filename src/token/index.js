const jwt = require("jsonwebtoken");

const accountsDb = require("../data-access/accounts/index")

const makeToken = require("./make-token")
const verifyToken = require("./verify-token")

const { encrypt } = require("../crypting/index")

const makeTokens = makeToken({ jwt })
const verifyTokens = verifyToken({jwt, accountsDb, encrypt})


const services = Object.freeze({
  makeTokens,
  verifyTokens
});
module.exports = services;
module.exports = {
  makeTokens,
  verifyTokens
};