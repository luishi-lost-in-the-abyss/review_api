const db = ({ dbs }) => {
    return Object.freeze({
        selectOneEmployee,
        insertEmployee
    })
 
    async function selectOneEmployee({ id, birthdate }) {
        const db = await dbs();
        const sql = "SELECT * FROM employees WHERE code=$1 AND birthdate=$2";
        const params = [id, birthdate];
        return db.query(sql, params);
    }

    async function insertEmployee(data) {
      
        const db = await dbs();
        const sql = `INSERT INTO employees (code, formalname, team, department, birthdate) VALUES ($1, $2, $3, $4, $5);`;
        const params = [
        data.code,
        data.formalname,
        data.team,
        data.department,
        data.birthdate
      ]
      return db.query(sql, params);
    }
    
  };
  module.exports = db;