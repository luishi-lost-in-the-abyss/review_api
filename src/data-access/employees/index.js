const makeDb = require("../index")

const db = require("./query")

const employeesDb = makeDb({ db })

module.exports = employeesDb