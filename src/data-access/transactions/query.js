const db = ({ dbs }) => {
    return Object.freeze({
      insertTransaction,
      selectTransactionByCode,
      selectAllTransactions
    });
 
    async function insertTransaction(data) {
      const db = await dbs();
      const sql = `INSERT INTO transactions (employeeid, date, answer, questionid) VALUES ($1, NOW(), $2, $3);`;
      const params = [
        data.employeeid,
        data.answer,
        data.questionid
      ]
      return db.query(sql, params);
    }

    async function selectTransactionByCode(code) {
      const db = await dbs();
      const sql = `SELECT * FROM transactions WHERE employeeid = $1;`;
      const params = [
        code
      ]
      return db.query(sql, params);
    }

    async function selectAllTransactions() {
      const db = await dbs();
      // const sql = `select b.formalname, b.code, c.question, a.questionid, a.answer from transactions a 
      // left join employees b on a.employeeid = b.code
      // left join questions c on a.questionid = c.id
      // order by a.id`;
      const sql =`select a.code, a.formalname, a.team, a.department, c.question, b.questionid, b.answer  from employees a
      left join transactions b on a.code = b.employeeid
      left join questions c on b.questionid = c.id
      order by b.id`
      return db.query(sql);
    }


  };
  module.exports = db;