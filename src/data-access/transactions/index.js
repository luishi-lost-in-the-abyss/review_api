const makeDb = require("../index")

const db = require("./query")

const transactionsDb = makeDb({ db })

module.exports = transactionsDb