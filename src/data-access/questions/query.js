const db = ({ dbs }) => {
    return Object.freeze({
      selectAllQuestions
    });
 

    async function selectAllQuestions() {
      const db = await dbs();
      const sql = "SELECT * FROM questions ORDER BY id";
      return db.query(sql);
  }
  
    
  };
  module.exports = db;