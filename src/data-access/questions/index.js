const makeDb = require("../index")

const db = require("./query")

const questionsDb = makeDb({ db })

module.exports = questionsDb