const db = ({ dbs }) => {
    return Object.freeze({
        insertAccount,
        selectCredentials,
        updateToken,
        changePassword
    })

    async function insertAccount(data) {
      
        const db = await dbs();
        const sql = `INSERT INTO accounts (employeeid, password) VALUES ($1, $2);`;
        const params = [
            data.employeeid,
            data.password
      ]
      return db.query(sql, params);
    }

    
    async function changePassword(data) {
      
        const db = await dbs();
        const sql = `UPDATE accounts SET password=$1
        WHERE employeeid=$2`;
        const params = [
            data.password,
            data.employeeid
      ]
      return db.query(sql, params);
    }

    async function selectCredentials(data){
        const db = await dbs();
        const sql = `SELECT * FROM accounts WHERE employeeid = $1 AND password = $2`;
        const params = [
            data.employeeid,
            data.password
      ]
      return db.query(sql, params);
    }

    async function updateToken(data){
        const db = await dbs();
        const sql = `UPDATE accounts SET token=$1
        WHERE employeeid=$2 AND password=$3;`;
        const params = [
            data.token,
            data.employeeid,
            data.password
      ]
      return db.query(sql, params);
    }
    
  };
  module.exports = db;