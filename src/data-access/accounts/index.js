const makeDb = require("../index")

const db = require("./query")

const accountsDb = makeDb({ db })

module.exports = accountsDb