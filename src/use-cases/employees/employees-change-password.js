const changePassword = ({
    accountsDb,
    encrypt,
    }) => {
      return async function put(info) {

        const checkPass = await accountsDb.selectCredentials({employeeid: info.employeeid, password: encrypt(info.oldPassword)})

        if(checkPass.rowCount === 0){
          throw new Error(`Incorrect password`)
        }


        const changePass = await accountsDb.changePassword({employeeid: info.employeeid, password: encrypt(info.newPassword)})
  
        return true
      
      };
    };
    module.exports = changePassword;