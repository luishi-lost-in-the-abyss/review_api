
const importEmployees = ({ employeesDb }) => {
  return async function post(employeesData) {

    let temp = []

    // console.log(employeesData.length)

    for (let i = 0; i < employeesData.length; i++) {
      await employeesDb.insertEmployee({
        code: employeesData[i].Code,
        formalname: employeesData[i].Formal_Name,
        team: employeesData[i].Team,
        department: employeesData[i].Department,
        birthdate: employeesData[i].Birth_Date_Processed
      })


      // temp.push({
      //   code: employeesData[i].Code,
      //   formalname: employeesData[i].Formal_Name,
      //   team: employeesData[i].Team,
      //   department: employeesData[i].Department,
      //   birthdate: employeesData[i].Birth_Date_Processed

      // })
    }




    return employeesData
  };
};

module.exports = importEmployees;
