
const employeesDb = require("../../data-access/employees/index");
const transactionsDb = require("../../data-access/transactions/index");
const accountsDb = require("../../data-access/accounts/index")

const { encrypt } = require("../../crypting/index")
const { makeTokens } = require("../../token/index")

const selectOneEmployee = require("./employees-select-one");
const importEmployees = require("./employees-import")
const employeeLogin = require("./employees-login")
const seedAdmin = require("./employees-seed-admin")
const changePassword = require("./employees-change-password")

const uc_selectOneEmployee = selectOneEmployee({ employeesDb, transactionsDb })
const uc_importEmployees = importEmployees({ employeesDb })
const uc_employeeLogin = employeeLogin({ accountsDb, encrypt, makeTokens })
const uc_seedAdmin = seedAdmin({ accountsDb, encrypt })
const uc_changePassword = changePassword({ accountsDb, encrypt })


const service = Object.freeze({
    uc_selectOneEmployee,
    uc_importEmployees,
    uc_employeeLogin,
    uc_seedAdmin,
    uc_changePassword
})

module.exports = service

module.exports = {
    uc_selectOneEmployee,
    uc_importEmployees,
    uc_employeeLogin,
    uc_seedAdmin,
    uc_changePassword
}
