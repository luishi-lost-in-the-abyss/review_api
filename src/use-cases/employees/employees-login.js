const employeeLogin = ({
  accountsDb,
  encrypt,
  makeTokens
  }) => {
    return async function put(info) {


        const account = await accountsDb.selectCredentials({employeeid: info.employeeid, password: encrypt(info.password)})

        if(account.rowCount === 0){
          throw new Error(`Incorrect password`)
        }

        const token = await makeTokens({
          employeeid: info.employeeid, password: info.password
        })


        const update = await accountsDb.updateToken({
          token: token,
          employeeid: info.employeeid,
          password: encrypt(info.password)
        })


        return {
          token: token,
          id: account.rows[0].employeeid
        }
    
    };
  };
  module.exports = employeeLogin;