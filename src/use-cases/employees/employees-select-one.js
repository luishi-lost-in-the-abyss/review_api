
const selectOneEmployee = ({ employeesDb, transactionsDb }) => {
    return async function get({id, birthdate}) {

      const result = await employeesDb.selectOneEmployee({id, birthdate})

      if(result.rowCount === 0){
        throw new Error(`Employee does not exist`)
      }

      let alreadyExist = await transactionsDb.selectTransactionByCode(id)

      if(alreadyExist.rowCount !== 0){
        throw new Error(`You have already completed this survey`)
      }

      const view = result.rows[0]

      return view
    };
  };
  
  module.exports = selectOneEmployee
  