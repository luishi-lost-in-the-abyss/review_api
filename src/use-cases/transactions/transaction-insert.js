
const insertTransaction = ({ transactionsDb }) => {
  return async function post({data}) {

    let code = data.code
 
    if(!code){
      throw new Error(`Code required`)
    }

    let alreadyExist = await transactionsDb.selectTransactionByCode(code)

    if(alreadyExist.rowCount !== 0){
      throw new Error(`You have already completed this survey`)
    }

    for(let i = 0; i < data.answers.length; i++){
      await transactionsDb.insertTransaction({employeeid: code, questionid: data.answers[i].questionid, answer: data.answers[i].answer})
    }
    
    return true
  };
};

module.exports = insertTransaction;
