const transactionsDb = require("../../data-access/transactions/index");



const insertTransaction = require("./transaction-insert");
const selectAllTransactions = require("./transactions-select-all")

const uc_insertTransaction = insertTransaction({ transactionsDb })
const uc_selectAllTransactions = selectAllTransactions({ transactionsDb })

const service = Object.freeze({
    uc_insertTransaction,
    uc_selectAllTransactions
})

module.exports = service

module.exports = {
    uc_insertTransaction,
    uc_selectAllTransactions
}
