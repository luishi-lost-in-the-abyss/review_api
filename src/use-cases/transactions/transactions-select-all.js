
const selectAllTransactions = ({ transactionsDb }) => {
    return async function get() {
        
      const result = await transactionsDb.selectAllTransactions()

      let data = result.rows

      let emps = []
      let qas = []

   

      for(let i = 0; i < data.length; i++){

        emps.push({
          employeeid: data[i].code,
          formalname: data[i].formalname,
          team: data[i].team,
          department: data[i].department
        })

   
        qas.push({
          employeeid: data[i].code,
          question: data[i].question,
          questionid: data[i].questionid,
          answer: data[i].answer
        })
       

      }

      const filteremps = filterArrayNoDuplicates(emps, "employeeid")


      for(let i = 0; i<filteremps.length; i++){

        let empid = filteremps[i].employeeid

        let dummyqas = []

        for(let i2 =0; i2<qas.length; i2++){

          if(empid === qas[i2].employeeid){
            dummyqas.push(qas[i2])
          }



        }

        if(!dummyqas[0].question){
          filteremps[i].questions = null
        }else{
        filteremps[i].questions = dummyqas
        }
      }




      return filteremps
    };
  };


  const filterArrayNoDuplicates = (arr, comp) => {
    const unique = arr
      .map(e => e[comp])
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
      // eliminate the dead keys & store unique objects
      .filter(e => arr[e])
      .map(e => arr[e]);
    return unique;
  };
  
  module.exports = selectAllTransactions
  