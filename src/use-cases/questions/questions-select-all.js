
const selectAllQuestions = ({ questionsDb }) => {
    return async function get() {
        
   
      const result = await questionsDb.selectAllQuestions()

      const view = result.rows

      return view
    };
  };
  
  module.exports = selectAllQuestions
  