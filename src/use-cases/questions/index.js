
const questionsDb = require("../../data-access/questions/index");

const selectAllQuestions = require("./questions-select-all");

const uc_selectAllQuestions = selectAllQuestions({ questionsDb })


const service = Object.freeze({
    uc_selectAllQuestions
})

module.exports = service

module.exports = {
    uc_selectAllQuestions
}
